﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavigation
{
    public partial class HierarchicalNavigationPage : ContentPage
    {
        public HierarchicalNavigationPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine("text: {text.Text}");
            if (text.Text == null || text.Text.Equals(""))
            {
                await DisplayAlert("Missing", "text", "Ok");
                return;
            }
            bool ret = await DisplayAlert("Warning", "Are you sure to go deeper ?", "Yes", "No");
            if (ret)
                await Navigation.PushAsync(new Page2(text.Text));
        }
    }
}
