﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchicalNavigation
{
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            text.Text = "LOL";
            InitializeComponent();
        }
        public Page2(string data)
        {
            InitializeComponent();
            text.Text = data;
        }
    }
}
